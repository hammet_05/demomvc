﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SobreflexMVC.Domain.Repository;
using SobreflexMVC.Domain.DTOs;
using SobreflexMVC.Data.Repositories;
using SobreflexMVC.Data;
namespace SobreflexMVC.Controllers
{
    public class UserController : Controller
    {
        //private IUserRepository IUserRepository=null;
        private UserRepository _UserRepostory=null;

        public UserController() 
        {
            _UserRepostory = new UserRepository();
        }
        //public UserController(IUserRepository userRepository)
        //{
        //    this.IUserRepository = userRepository;
        //}
        // GET: /User/
        public ActionResult Index()
        {
            var lUsers = new List<User>();
            lUsers = (List<User>)_UserRepostory.SelectAll();
            ViewBag.Users = lUsers;
            return View("Index");
        }

        //
        // GET: /User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /User/Create
        public ActionResult Create()
        {
            return View();
        }
        // GET: /User/Forgot
        public ActionResult Forgot()
        {
            return View();
        }
        //GET: /User/Edit
        public ActionResult Query()
        {
            return View();
        }



        //
        // POST: /User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
