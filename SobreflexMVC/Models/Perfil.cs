﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SobreflexMVC.Models
{
    public class Perfil
    {
        public int IdPerfil { get; set; }
        public string NombrePerfil { get; set; }
    }
}