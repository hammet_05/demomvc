﻿

using System.Data.Entity;

namespace SobreflexMVC.Models
{
    public class SobreflexContext: System.Data.Entity.DbContext
    {
        public SobreflexContext() { }

        public DbSet<Usuario> Usuarios { get; set; }

        public System.Data.Entity.DbSet<SobreflexMVC.Domain.DTOs.User> Users { get; set; }
    }
   
}