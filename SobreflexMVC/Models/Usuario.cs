﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SobreflexMVC.Models
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string NumDocumento { get; set; }
        public string Email { get; set; }
        public int IdPerfil { get; set; }
        public string Password { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Estado { get; set; }
        public string Telefono { get; set; }
        public bool PasswordBloqueado { get; set; }
        public bool PasswordTemporal { get; set; }
        public DateTime? UltimoAcceso { get; set; }
        public int? IdTipoIps { get; set; }
        public bool? RegistroWeb { get; set; }
        public DateTime? FechaInactivacion { get; set; }

        public Perfil IdPerfilNavigation { get; set; }
    }
}