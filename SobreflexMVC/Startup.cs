﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SobreflexMVC.Startup))]
namespace SobreflexMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
