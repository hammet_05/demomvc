﻿using System.Web;
using System.Web.Optimization;

namespace SobreflexMVC
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, consulte http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/bundles/Content/css").Include("~/Content/bootstrap.css", "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/Assets/css")
                        .Include("~/Assets/vendor/bootstrap/css/bootstrap.css", //Bootstrap core CSS,                                  
                                 "~/Assets/vendor/fontawesome-free/css/all.min.css",//Custom fonts for this template
                                 "~/Assets/vendor/datatables/dataTables.bootstrap4.css", //Page level plugin CSS
                                 "~/Assets/sb-admin.css"//Custom styles for this template,
                                 ));

            bundles.Add(new ScriptBundle("~/bundles/Assets/js")
                        .Include("~/Assets/vendor/jquery/jquery.js",
                                 "~/Assets/vendor/bootstrap/js/bootstrap.bundle.min.js",
                                 "~/Assets/vendor/jquery-easing/jquery.easing.min.js",
                                 //"~/Assets/vendor/chart.js/Chart.min.js",
                                 "~/Assets/vendor/datatables/jquery.dataTables.js",
                                 "~/Assets/vendor/datatables/dataTables.bootstrap4.js",
                                 "~/Assets/vendor/fontawesome-free/css/all.min.css",
                                 "~/Assets/js/sb-admin.min.js",
                                 "~/Assets/js/demo/datatables-demo.js",
                                 "~/Assets/js/demo/chart-area-demo.js"                                 
                                ));


            BundleTable.EnableOptimizations = false;
        }
    }
}
