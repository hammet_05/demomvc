﻿using SobreflexMVC.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SobreflexMVC.Domain.Repository
{
    public interface IUserRepository
    {
        IEnumerable<User> SelectAll();

       
    }
}
