﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SobreflexMVC.Domain.Repository;
using SobreflexMVC.Domain.DTOs;


namespace SobreflexMVC.Data.Repositories
{
    public class UserRepository:IUserRepository
    {
       
        public UserRepository()
        {
           
        }
       

        public IEnumerable<User> SelectAll()
        {
            var lUsers = new List<User>();
            try
            {
                using (var context = new SobreflexDataContext())
                {
                    var listaUsers = (from users in context.SF_Usuario
                                      where users.Estado == true && users.IdTipoIPS != 5
                                      select new
                                      {
                                          users.Nombre,
                                          users.Email,
                                          users.NumDocumento
                                      }).ToList();
                    foreach (var item in listaUsers)
                    {
                        var user = new User();
                        user.Name = item.Nombre;
                        user.Email = item.Email;
                        user.Identification = item.NumDocumento;

                        lUsers.Add(user);
                    }
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lUsers;
        }
        
    }
}