﻿
namespace SobreflexMVC.Data
{
    public class Connection
    {
        public SobreflexDataContext Context = new SobreflexDataContext();

        public Connection() { }

        public Connection(SobreflexDataContext context)
        {
            Context = context;
        }

        
    }
}